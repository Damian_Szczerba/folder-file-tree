'''
Given an absolute path, write a program that outputs an ASCII tree of that directory.
'''

import os


class Tree:
    def __init__(self, path):
        self.absolute_path = path
        self.path_length = len(self.absolute_path)
        self.go_through_folder(self.absolute_path, 0)

    def go_through_folder(self, absolute_path, num_spaces):

        # Go through each item in a given path
        for element in os.listdir(absolute_path):
            full_path = "%s\\%s" % (absolute_path, element)

            # If the item is a file, print the name else print the name and use recursion and access the folder
            if os.path.isfile(full_path):
                spaces = "|   " * (num_spaces // 5)
                file_name = "%s-%s" % (spaces, element)
                print(file_name)

            elif os.path.isdir(full_path):
                spaces = "|   " * (num_spaces // 5)
                folder_name = "%s+%s" % (spaces, element)
                print(folder_name)
                self.go_through_folder(full_path, num_spaces + 5)

Tree("absolute path here")
